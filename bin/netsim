#!/usr/bin/env python
"""an executable for quickly testing things
"""
__author__ = "Chris Pankow <chris.pankow@ligo.org>"

#-------------------------------------------------

import os
import math

from functools import partial

import numpy
from scipy.interpolate import interp1d
from scipy.integrate import trapz
from scipy.optimize import brentq

import lal
_detectors = dict([(d.frDetector.prefix, d) for d in lal.CachedDetectors])
import lalsimulation

from gw_event_gen import eventgen
from gw_event_gen import netsim

import matplotlib
matplotlib.use("agg")
from matplotlib import pyplot
from scipy.interpolate import interp1d

#-------------------------------------------------

aligo = netsim.Network(("H1",))

# net Theta^2 testing
x = numpy.linspace(0, 1, 100)
pyplot.plot(x, netsim._fit_theta(x), label="analytic")

zh = aligo.redshift_horizon(mass1=10., mass2=10.)["H1"]
y = netsim._fit_theta(x) * aligo.snr_correction(mass1=10., mass2=10.0)(x * zh)
pyplot.plot(x, y, label="analytic, 10+10 corrected")

th_sq, ccdf = aligo._theta_sq_net()
pyplot.plot(x, netsim._fit_theta(x, x[0], x[-1], interp1d(th_sq, ccdf)), label="1 det")

aligo.add_psd("L1")
th_sq, ccdf = aligo._theta_sq_net()
pyplot.plot(x, netsim._fit_theta(x, x[0], x[-1], interp1d(th_sq, ccdf)), label="2 det")

aligo.add_psd("V1")
th_sq, ccdf = aligo._theta_sq_net()
pyplot.plot(x, netsim._fit_theta(x, x[0], x[-1], interp1d(th_sq, ccdf)), label="3 det")

pyplot.legend()
#pyplot.savefig("net_resp.png")

#-------------------------------------------------
# Usage and unit testing
#-------------------------------------------------

network = netsim.Network(("H1", "L1", "V1"))

# Generate non-spinning NSBH in comoving volume
generator = eventgen.EventGenerator()

generator.append_generator(eventgen.uniform_nsbh_comp_mass)
generator.append_generator(eventgen.ZeroSpin())
generator.append_generator(eventgen.UniformComoving(z_max=0.5))
generator.append_generator(eventgen.RandomOrientation())
generator.append_generator(eventgen.ZeroEccentricity())
generator.append_generator(eventgen.UniformEventTime(t0=1e9, dur=0)

# Add post filters
generator.append_transform(eventgen.ComponentMass2MchirpEta())

for e in generator.generate_events(n_itr=1):
    print(e.mass1, e.eta, e.distance, e.inclination)
    e.waveform()

    print(network.snr(e))

#generator.to_hdf5("test.hdf", "events")
#generator.to_xml("test.xml.gz")
