from __future__ import print_function

"""a module that mocks up detector networks and supplies various utility functions associated therewith
"""
__author__ = "Chris Pankow <chris.pankow@ligo.org>"

#-------------------------------------------------

import os
import math

import numpy as np
from scipy.interpolate import interp1d
from scipy.integrate import trapz
from scipy.optimize import brentq

import lal
_detectors = dict([(d.frDetector.prefix, d) for d in lal.CachedDetectors])
import lalsimulation

from . import eventgen

#-------------------------------------------------
# General utilities
#-------------------------------------------------

def list_instruments():
    for prefix in sorted(_detectors):
        inst = _detectors[prefix].frDetector.name
        psd = \
            _default_psds[prefix if prefix in _default_psds else None].__name__
        print("%s (%s): default PSD: %s" % (inst, prefix, psd))

#------------------------

def _to_lal_cmplx_fs(np_ar):
    _unit = lal.StrainUnit * lal.SecondUnit
    fs = lal.CreateCOMPLEX16FrequencySeries("", lal.LIGOTimeGPS(0.0), 0., 0.125, _unit, len(np_ar))
    fs.data.data = np_ar.copy()
    return fs

def _to_lal_real_fs(np_ar):
    _unit = lal.StrainUnit ** 2 * lal.SecondUnit
    fs = lal.CreateREAL8FrequencySeries("", lal.LIGOTimeGPS(0.0), 0., 0.125, _unit, len(np_ar))
    fs.data.data = np_ar.copy()
    return fs

#-------------------------------------------------
# Analytic volume utilities
#-------------------------------------------------

# Table I, Finn and Chernoff, 1993
_theta_sqr = np.array(
    [
        [100.0,  0.0],
        [ 90.0,  0.240],
        [ 80.0,  0.542],
        [ 75.0,  0.707],
        [ 70.0,  0.878],
        [ 60.0,  1.250],
        [ 50.0,  1.709],
        [ 40.0,  2.283],
        [ 30.0,  3.020],
        [ 25.0,  3.485],
        [ 20.0,  4.063],
        [ 10.0,  6.144],
        [  9.0,  6.471],
        [  8.0,  6.832],
        [  7.0,  7.239],
        [  6.0,  7.701],
        [  5.0,  8.233],
        [  4.0,  8.857],
        [  3.0,  9.614],
        [  2.0, 10.589],
        [  1.0, 11.985],
        [  0.5, 13.054],
        [  0.4, 13.350],
        [  0.3, 13.682],
        [  0.2, 14.091],
        [  0.1, 14.284],
        [  0.0, 16.000],
    ],
    dtype=float,
)
_theta_sq = interp1d(_theta_sqr[:,1]/16., _theta_sqr[:,0]/100.)

def _fit_theta(values, vmin=None, vmax=None, theta_sq_intrp=_theta_sq):
    vmin = vmin if vmin is not None else np.min(values)
    vmax = vmax if vmax is not None else np.max(values)
    _val_scaled = (values - vmin) / (vmax - vmin)
    # CDF already has 16 scaled out
    return theta_sq_intrp(_val_scaled**2)

def theta(det, time, ra, dec, pol, incl):
    """
    Amplitude orientation scaling factor, see Finn and Chernoff, 1996, eqn. 3.31. Note that this returns theta, not theta^2. So the range of this variable is 0 <= theta <= 4.

    >>> round(theta("H1", 1e9, 0., 0., 0., 0.,), 5)
    1.17058
    """
    rp = lal.cached_detector_by_prefix[det].response
    t = lal.GreenwichMeanSiderealTime(lal.LIGOTimeGPS(time))
    fp, fx = lal.ComputeDetAMResponse(rp, ra, dec, pol, t)
    cos_inc2 = np.cos(incl)**2
    return 2 * np.sqrt(fp**2 * (1 + cos_inc2)**2 + 4 * fx**2 * cos_inc2)

#-------------------------------------------------
# Mock PSDs and PSD parsing
#-------------------------------------------------

# Apply fudge factor of 2 (in ASD) to mock up A+
def aplus_mock_psd(freq):
    return lalsimulation.SimNoisePSDaLIGOZeroDetHighPower(freq) / 4

_MOCK_PSDS = {
    "aplus_mock_psd": aplus_mock_psd,
}

# FIXME: Switch to lalsimulation version when it becomes available
try:
    d = os.path.dirname(os.path.abspath(__file__))
    fname = os.path.join(d, "LIGO-T1800042-v5-aLIGO_APLUS.txt")
    f, a = np.loadtxt(fname, unpack=True)
    _APLUS_INTERP = interp1d(f, a**2)
    def aplus_design_psd(freq):
        return _APLUS_INTERP(freq)

    _MOCK_PSDS["aplus_design_psd"] = aplus_design_psd
except IOError:
    pass

_default_psds = {
    "H1": lalsimulation.SimNoisePSDaLIGOZeroDetHighPower,
    "I1": lalsimulation.SimNoisePSDaLIGOZeroDetHighPower,
    "K1": lalsimulation.SimNoisePSDKAGRA,
    "L1": lalsimulation.SimNoisePSDaLIGOZeroDetHighPower,
    "V1": lalsimulation.SimNoisePSDAdvVirgo,
    None: lalsimulation.SimNoisePSDaLIGOZeroDetHighPower
}

def parse_psd(name, freqar=None, asd=False):
    freqar = np.arange(0, 2048.125, 0.125) if freqar is None else freqar

    if name in dir(lalsimulation):
        fcn = getattr(lalsimulation, name)
        # No way to get the arity of builtins
        # So... guess and check
        try:
            # Prebuilt mapping
            p = list(map(fcn, freqar))
        except TypeError:
            # fill from file
            psdar = _construct_psd(np.zeros(freqar.shape), freqar[1] - freqar[0])
            fcn(psdar, freqar[0])
            p = psdar.data.data
    elif name in _MOCK_PSDS:
        p = list(map(_MOCK_PSDS[name], freqar))
    elif os.path.exists(name):
        f, p = np.loadtxt(name, unpack=True)
        p = interp1d(f, p, fill_value=np.inf, bounds_error=False)(freqar)
    else:
        raise ValueError("No pregenerated PSD or PSD file found for %s" % name)

    if asd:
        p *= p
    return np.asarray(p)

def _construct_psd(data, delta_f):
    psd = lal.CreateREAL8FrequencySeries('', lal.LIGOTimeGPS(0), 0., delta_f,  lal.StrainUnit, len(data))
    psd.data.data = data
    return psd

def _construct_psd_from_func(func, delta_f, fhigh):
    return _construct_psd(list(map(func, np.linspace(0, fhigh, delta_f))), delta_f)

#-------------------------------------------------
# Detector Networks
#-------------------------------------------------

class Network(object):
    def __init__(self, instruments=tuple()):
        """
        Initialize the object, optionally providing instrument prefixes which will be added to the object with their default PSDs.
        """
        self._instr = {}
        self._dc = {}
        self._active = {}
        self._psd_cache = {}
        for inst in instruments:
            self.add_psd(inst)

    def __repr__(self):
        return ", ".join(list(self._instr.keys()))

    """
    def get_psd(self, ifo)
        if type == func:
            pass
        return
    """

    def add_psd(self, inst, psd=None, duty_cycle=1.0, asd=False, delta_f=0.125):
        """
        Add an instrument to network characterized with a given PSD.
        """
        if psd is None:
            psd = np.asarray(list(map(_default_psds[inst], \
                                        np.arange(0, 2048.125, 0.125))))
        elif callable(psd):
            if asd:
                psd = np.asarray(map(psd, \
                                        np.arange(0, 2048.125, 0.125))**2)
            else:
                psd = np.asarray(list(map(psd, np.arange(0, 2048.125, 0.125))))
        else:
            psd = parse_psd(psd, np.arange(0, 2048.125, 0.125), asd)

        self._instr[inst] = psd
        self._dc[inst] = duty_cycle

    def del_psd(self, inst):
        """
        Remove an instrument from the network.
        """
        del self._instr[inst]
        del self._dc[inst]

    def set_dc_mask(self):
        self._active = dict([(inst, self._dc.get(inst, 1.0) > rnd) \
                            for inst, rnd in \
                zip(self._instr, np.random.uniform(size=len(self._instr)))])

    def unset_dc_mask(self):
        self._active = {}

    def _serialize(self):
        """
        Convert the network object into a dictionary.
        """
        obj = {}
        for inst in self._instr:
            obj[instr] = (self._instr[inst], self._dc[inst])

    @classmethod
    def _deserialize(cls, obj, **kwargs):
        """
        Convert a dictionary into a network object.
        """
        net = Network()
        for inst, tup in obj.items():
            net.add_psd(inst, tup[0], tup[1], **kwargs)
        return net

    def _invalidate_psd_cache(self):
        self._psd_cache = {}

    def horizon(self, snr_threshold=8, ifos=None, fmin=10., fmax=2048., **kwargs):
        """
        Determine the horizon distance: Euclidean distance at which a source with the given parameters, optimally oriented, for an event gives the snr value of snr_threshold.

        Does not correct for redshift, see `redshift_horizon` for this functionality.
        """
        # explicitly override distance to known value, just in case
        # FIXME: astropy units, please
        kwargs["distance"] = 1. # Mpc

        tmplt = eventgen.Event(**kwargs)
        horizon_dict = self.snr(tmplt, ifos, fmin, fmax, optimal=True)
        for inst in horizon_dict:
            snr = horizon_dict[inst]
            # Assumes distance factor is unity
            horizon_dict[inst] = snr / snr_threshold

        return horizon_dict

    def _theta_sq_net(self, by_ifo=False, **kwargs):
        """
        Compute the average amplitude distribution for the network.
        """
        from .eventgen import theta
        rvs = []
        for _ in range(10000):
            ra, dec, incl, pol, _ = eventgen.random_orientation()
            # FIXME: probably not necessary?
            t = np.random.uniform(1e9, 1e9 + 86400 * 365)
            th_sq = np.square([theta(ifo, kwargs.pop("t", t), \
                                             kwargs.pop("ra", ra), \
                                             kwargs.pop("dec", dec), \
                                             kwargs.pop("pol", pol), \
                                             kwargs.pop("incl", incl)) \
                                    for ifo in self._instr])
            if not by_ifo:
                th_sq = th_sq.sum()
            rvs.append(th_sq)

        if by_ifo:
            return np.asarray(rvs) / 16.

        # FIXME: is scaling by number of detectors right?
        rvs = np.asarray(sorted(rvs)) / 16. / len(self._instr)

        rvs = np.concatenate(([0], rvs, [1]))
        return rvs, 1 - np.linspace(0, 1, len(rvs))

    def snr_correction(self, snr_threshold=8, fmin=10., fmax=2048., **kwargs):
        """
        The original pdet(r/r_h) derived from the amplitude distirbution function in Finn and Chernoff doesn't quite hold for cosmological distances. This attempts a correction by recalculating the SNR with redshifting against the fiducial threshold and producing a correction to p_det.
        """
        from astropy.cosmology import Planck15, z_at_value
        mass1, mass2 = kwargs.get("mass1", None), kwargs.get("mass2", None)

        zs = kwargs.get("zbins", None)
        zbins_provided = zs is not None
        if not zbins_provided:
            zh = self.redshift_horizon(mass1=mass1, mass2=mass2,
                                            snr_threshold=snr_threshold)["H1"]

            zs = np.linspace(1e-4, zh, 50)
        
        dl = Planck15.luminosity_distance(zs).value

        snr_redshift = []
        for z, d in zip(zs, dl):
            _m1, _m2 = mass1 * (1 + z), mass2 * (1 + z)
            tmplt = eventgen.Event(mass1=_m1, mass2=_m2, \
                                                    distance=d)
            snr_redshift.append(self.snr_at_ifo(tmplt, ifo="H1", optimal=True))

        # This is the correction to pdet
        ratio = 1 - np.divide(snr_threshold, snr_redshift)**2

        # The user asked for specific zs, so we return the array of the same
        # shape evaluated at those zs
        if zbins_provided:
            return ratio

        # Manually set first element to zero redshift, unity correction
        zs[0], ratio[0] = 0., 1.
        return interp1d(zs, ratio)

    def analytic_volume(self, snr_threshold=8, fmin=10., fmax=2048.,
                                redshift_correction=False, **kwargs):
        """
        Fixed mass/spin analytic detection weighted sensitivity volume. Currently uses Planck15 cosmology from astropy. Returns volume in Gpc^3.
        """
        import sys
        from astropy.cosmology import Planck15
        from scipy.integrate import fixed_quad
        print("This function is mostly untested, and uses the horizon of the most sensitive detector rather than trying to figure out the correct horizon threshold. Its preliminary tests show agreement at the 10% level with other sources.", file=sys.stderr)
        # FIXME: use whole network
        #z_h = self.redshift_horizon(snr_threshold, fmin, fmax, **kwargs)
        z_h = max(self.redshift_horizon(snr_threshold, fmin=fmin, fmax=fmax, \
                    **kwargs).values())

        # Correct p_det for knock on effect in SNR against redshift masses
        if redshift_correction:
            snr_correction = self.snr_correction(snr_threshold, fmin, fmax, \
                                                    **kwargs)
        else:
            snr_correction = lambda z: 1.

        def _intg(z):
            corr = snr_correction(z)
            return _fit_theta(z, vmin=0, vmax=z_h) / (1 + z) * \
                        Planck15.differential_comoving_volume(z).value \
                        * corr
        return fixed_quad(_intg, 0, z_h)[0] / 1e9 * 4 * np.pi

    def pop_averaged_volume(self, distr_func, snr_threshold=8, fmin=10., \
                                fmax=2048., z_max=2, network=False, **kwargs):
        """
        Determine the redshift horizon for a population of events with a given snr_threshold. Requres an iterable for distr_func, in which each element of the iterable provides a set intrinsic parameters (currently just mass1 and mass2). The detection probability is assumed to follow the Finn & Chernoff theta^2 function CCDF.

        If network=False, this will use the horizon and detection probability functions for a single instrument (the horizon being the maximum over all instruments). If network=True, the snr_threshold is assumed to be the *network* SNR threshold, and the detection probability is recalculated to account for correlations between the instruments in the network.

        Currently uses Planck15 cosmology from astropy.
        """

        # Import here in case people don't have/want astropy
        from astropy import units
        from astropy.cosmology import Planck15, z_at_value

        u = np.linspace(0, z_max, 1000)

        # Volume factor
        dVdu = Planck15.differential_comoving_volume(u)
        dVdu = 4 * np.pi * dVdu.to("Gpc^3 / sr").value / (1 + u)

        # Find the expectation value of the redshifted detection function
        favg = np.zeros(u.shape)

        # Choose whether to use the (correlated) network amplitude distribution
        # or a single detector
        # FIXME: Add SNR correction
        pdet = interp1d(*self._theta_sq_net()) if network else _theta_sq

        #
        # MC integral version
        #
        n = 0
        for e in distr_func:
            # For now, just masses
            # FIXME: Need to check the horizon for the network
            # FIXME: This is the slowest step in the loop --- could probably
            # use an interpolation table, but that will get messy in high
            # dimensions
            zh = max(self.redshift_horizon(mass1=e.mass1, mass2=e.mass2, \
                                        snr_threshold=snr_threshold).values())
            assert zh <= z_max, "Found an event with horizon redshift ({0:f}) beyond the assumed maximum ({1:f}). Masses: {2:.1f} {3:.1f}".format(zh, z_max, e.mass1, e.mass2)

            # What interval is our detection efficiency defined on
            valid = u < zh
            if len(u[valid]) / float(len(u)) < 0.01:
                print("Warning, volume calculation is using less than 1% of the total redshift range. Inaccuracies could result.")

            # Horizon-scaled detection probability: f(u * z_h / z_max)
            favg[valid] += _fit_theta(u[valid], pdet)

            n += 1

        favg *= dVdu
        favg /= n

        # If requested, clip the interval
        clip = kwargs.pop("z_max_clip", None)
        if clip is not None:
            clip = u < clip
            u, favg = u[clip], favg[clip]

        diff = kwargs.pop("differential", None)
        if diff is not None:
            return u, favg

        # final integral over scaled redshifts
        return trapz(favg, u)


    def redshift_horizon(self, snr_threshold=8, ifos=None, \
                            fmin=10., fmax=2048., **kwargs):
        """
        Determine the redshift horizon for an event with a given snr_threshold. This effectively searches for the redshift at which the combination of the redshifted masses and luminosity distance provide an SNR of 8 in a given instrument. Spot checks have been performed against this (more complete) reference: https://arxiv.org/abs/1709.08079

        Currently uses Planck15 cosmology from astropy.
        """

        # Import here in case people don't have/want astropy
        from astropy import units
        from astropy.cosmology import Planck15, z_at_value

        # Horizon distance in Euclidean space
        # FIXME: Horizon needs a snr argument, assumes 8
        dh_euclid = max(self.horizon(snr_threshold, ifos, fmin, fmax, \
                            **kwargs).values())
        z_euclid = \
            z_at_value(Planck15.luminosity_distance, dh_euclid * units.Mpc)

        # User-supplied choice of cosmology calculator. Default is Planck15
        # lal is also minimally supported.
        cosmo = kwargs.pop("cosmology", "Planck15")
        if cosmo == "lal":
            cprms = lal.CosmologicalParameters()
            lal.SetCosmologicalParametersDefaultValue(cprms)
        else:
            cprms = None

        # Readjust source masses until SNR is at correct value again
        def _delta_snr(z, ifo):
            tmplt_args = kwargs.copy()
            if cosmo == "Planck15":
                tmplt_args["distance"] = \
                    Planck15.luminosity_distance(z).to("Mpc").value
            elif cosmo == "lal":
                tmplt_args["distance"] = lal.LuminosityDistance(cprms, z)
            tmplt_args["mass1"] *= 1 + z
            tmplt_args["mass2"] *= 1 + z
            tmplt = eventgen.Event(**tmplt_args)
            # NOTE: Some care is needed here. If there is more than one ifo in
            # the network, 'optimal' is not appropriate --- there's basically
            # no way to make an event optimal in more than one instrument
            # simultaneously
            return self.snr_at_ifo(tmplt, ifo, optimal=True) - snr_threshold

        # FIXME: Better bounds can be inferred from masses
        z_horizon = {}
        for ifo in ifos or self._instr:
            z_horizon[ifo] = brentq(_delta_snr, \
                                z_euclid * 1e-3, z_euclid * 10, args=(ifo,))
        return z_horizon

    def snr_at_ifo(self, event, ifo, fmin=10., fmax=2048., optimal=False):
        """
        Calculate the matched filtered SNR for this event at a specific instrument. This basically inlines a dictionary lookup.
        """
        return self.snr(event, (ifo,), fmin, fmax, optimal)[ifo]

    def snr(self, event, ifos=None, fmin=10., fmax=2048., optimal=False, **kwargs):
        """
        Calculate the matched filtered SNR for this event over the set of instruments "ifos". If ifos is None, then do so over the entire defined network.
        """
        _remove_wf = False
        if event.hp is None:
            event.waveform(**kwargs)
            _remove_wf = True

        snrs = {}
        for ifo in ifos or self._instr:
            if not self._active.get(ifo, True):
                snrs[ifo] = 0.
                continue

            # Make copy to manipulate
            _hp = _to_lal_cmplx_fs(event.hp.data.data)
            det = _detectors[ifo]

            # Cache PSD series for speed --- call _invalidate_cache to clear
            if ifo not in self._psd_cache:
                psd = self._psd_cache[ifo] = _to_lal_real_fs(self._instr[ifo])
            else:
                psd = self._psd_cache[ifo]

            if not optimal:
                gmst = lal.GreenwichMeanSiderealTime(lal.LIGOTimeGPS(event.time))
                fp, fx = lal.ComputeDetAMResponse(det.response, \
                        event.right_ascension, event.declination, \
                        event.polarization, gmst)
            else:
                fp, fx = 1., 0.

            _hp.data.data = fp * event.hp.data.data + fx * event.hx.data.data
            snrs[ifo] = lalsimulation.MeasureSNRFD(_hp, psd, fmin, fmax)

        if _remove_wf:
            event.del_waveform()

        return snrs

    def net_snr(self, event, ifos=None, fmin=10., fmax=2048., optimal=False, \
                    **kwargs):
        """
        Calculate the matched filter *network* SNR for all instruments in `ifos`. If this is not provided, use the internal instrument list.
        """
        snrs = np.asarray(list(self.snr(event, ifos, fmin, fmax, \
                                        optimal, **kwargs).values()))
        return math.sqrt(sum(snrs**2))

    def antenna(self, event):
        ant = {}
        for ifo in self._instr:
            resp = lal.cached_detector_by_prefix[ifo]
            gmst = \
                lal.GreenwichMeanSiderealTime(lal.LIGOTimeGPS(event.time))
            fp, fx = lal.ComputeDetAMResponse(resp.response, \
                                              event.right_ascension, \
                                              event.declination, \
                                              event.polarization, gmst)
            ant[ifo] = (fp, fx)

        return ant

    def sum_sq_antenna(self, event, average=False):
        ant = self.antenna(event)
        ant = np.square(list(ant.values())).sum()
        if average:
            return np.sqrt(ant) / n
        return np.sqrt(ant)
