import numpy as np

from .event import Event
from .distributions import (TimeDistribution, MassDistribution, \
                            SpinDistribution, OrientationDistribution, \
                            EccentricityDistribution, RedshiftDistribution, \
                            DistanceDistribution)
from .transforms import (SourceMass2DetectorMass, Redshift2LuminosityDistance, \
                         LuminosityDistance2Redshift)

from .io import *

#---------------------------------------------------------------------------------------------------
# EventGenerator classes
#---------------------------------------------------------------------------------------------------

class EventGenerator(object):
    """a workhorse object that wraps parameter distributions together to generate events
    """

    def __init__(self, generators=[], transforms=[], conditionals=[]):
        self._events = []

        ### list of things used to generate event parameters
        self._generators = []
        for gen in generators:
            self.append_generator(gen)
        
        ### list of transofmrations to apply when events are generated
        self._transforms = []
        for trans in transforms:
            self.append_transform(trans)

        # These are functions called to do rejection sampling of generated events.
        self._conditionals = []
        for cond in conditionals:
            self.append_conditional(cond)

    @property
    def generators(self):
        return self._generators

    @property
    def transforms(self):
        return self._transforms

    @property
    def conditionals(self):
        return self._conditionals

    def __iter__(self):
        for e in self._events:
            yield e

    def __len__(self):
        return len(self._events)

    def extend(self, egen):
        self._events.extend(egen._events)

    def append(self, event):
        self._events.append(event)

    @staticmethod
    def _check_required(present, required):
        for variate in required:
            assert variate in present, 'required=%s not present!'%variate

    @staticmethod
    def _check_variates(present, variates):
        for variate in variates:
            assert variate not in present, 'variate=%s already produced!'%variate

    @property
    def generator_variates(self):
        variates = ()
        for gen in self.generators:
            variates += gen._variates
        return variates

    @property
    def transform_variates(self):
        variates = ()
        for trans in self.transforms:
            variates += trans._variates
        return variates

    def _check_generator(self, obj):
        """check to make sure we don't already have any generator that will generate one of obj._variates and that we have a generator that will produce obj._required"""
        variates = self.generator_variates
        self._check_variates(variates, obj._variates)
        self._check_required(variates, obj._required)

    def append_generator(self, obj):
        """Add a generator for a set of properties of the event. Propety names are specified by `lbls`, the generator function is `gen`, and `kwargs` is bound to the function via `functools.partial`. This is useful if the generator function itself has arguments which need to be set, e.g. maximum masses, ranges, etc..."""
        self._check_generator(obj)
        self._generators.append(obj)

    def _check_transform(self, obj):
        """check to make sure we don't already have a generator or transform that will generate one of obj._variates and that we have a generator or transform that will produce obj._required"""
        variates = self.generator_variates + self.transform_variates
        self._check_variates(variates, obj._variates)
        self._check_required(variates, obj._required)

    def append_transform(self, obj):
        """Add a post-generator function for a set of properties of the event. Typically, this is for properties which are not controlled by the generator function, but are instead derived (e.g. total mass from component masses). Propety names are specified by `lbls`, the post function is `flt`, and `kwargs` is bound to the function via `functools.partial`. This is useful if the post filter function itself has arguments which need to be set.
        """
        self._check_transform(obj)
        self._transforms.append(obj)

    def append_conditional(self, cond, **kwargs):
        """Add a conditional function when generating properties of the event. This is useful to allow for a type of rejection sampling. \
An example would be rejecting events whose SNR falls below a certain threshold. The conditional function is `cond`, and `kwargs` is bound to \
the function via `functools.partial`. This is useful if the conditional itself has arguments which need to be set.
        """
        if kwargs:
            self._conditionals.append(partial(cond, **kwargs))
        else:
            self._conditionals.append(cond)

    def generate_events(self, n_itr=np.infty, n_event=np.infty, verbose=False, yield_all=False):
        """Loop and generate events.
        """
        tries = 0
        while (tries < n_itr) and (len(self._events) < n_event): ### stop when we hit one of these limits
            tries += 1
            event = Event()

            #if verbose is True or tries % verbose == 0:
            if verbose:
                print("{0:d} / {1}: {2:d} / {3}".format(tries, str(n_itr), len(self._events), str(n_event)))

            # Generate user-specified randomized properties
            for gen in self._generators:
                ### size is always the first argument!
                rv = gen.rvs(1, *[getattr(event, variate) for variate in gen._required])[0]
                for l, v in zip(gen._variates, rv):
                    setattr(event, l, v)

            # Generate "dependent" properties as transformations over the base properties
            for trans in self._transforms:
                trans(event)

            # Check that the event passes checks
            check = True
            for func in self._conditionals:
                check &= func(event)

            ### finish processing event
            if yield_all:
                yield check, event

            elif check:
                yield event

            if not check:
                continue

            self._events.append(event)

        if verbose:
            print("%d events generated, %d kept." % (tries, len(self._events)))

    def pdf(self, event, store=False):
        """evaluate the sampling pdf for the variates associated with this event
        """
        pdf = 1.
        for gen in self._generators:
            pdf *= gen.pdf(*[getattr(event, variate) for variate in gen._variates+gen._required])
        if store:
            setattr(event, 'pdf', pdf)
        return pdf

    def flush(self):
        self._events = []

    def update(self):
        """This applies all post filters on the event set. This may be useful in cases where the event list is statically generated, and the user wants a new set of columns that wasn't accessible during generation.
        """
        # Regenerate "dependent" properties via transforms
        for event in self._events:
            for trans in self._transforms:
                trans(event) ### this modifies the event in place

    @property
    def attributes(self):
        attrs = set()

        for gen in self._generators:
            attrs |= set(gen._variates)
        for trans in self._transforms:
            attrs |= set(trans._variates)

        return attrs

    ### I/O wrappers

    def write2csv(self, *args, **kwargs):
        events2csv(self._events, self.attributes, *args, **kwargs)

    def write2xml(self, *args, **kwargs):
        events2xml(self._events, self.attributes, *args, **kwargs)

    def write2hdf5(*args, **kwargs):
        events2hdf5(self._events, self.attributes, *args, **kwargs)

#------------------------

class MonteCarloIntegrator(EventGenerator):
    """an object that implements smart termination conditions for approximating Monte-Carlo integrals of VT
    """

    _required = Event._required

    def __init__(self, *args, **kwargs):
        EventGenerator.__init__(self, *args, **kwargs)
        self._check_required(self.generator_variates+self.transform_variates, self._required)

        Nparams = sum(len(gen._params) for gen in self._generators)
        self._e1 = 0.
        self._e2 = 0.
        self._de1 = np.zeros(Nparams, dtype=float)
        self._de2 = np.zeros(Nparams, dtype=float)

    def flush(self):
        EventGenerator.flush(self)
        self._e1 = 0.
        self._e2 = 0.
        self._de1[:] = 0.
        self._de2[:] = 0.

    def update_max_redshift(self, network):
        """update the maximum redshift or distance defined in generator to avoid sampling too distant sources

    **NOTE**: right now, we just skip this and leave the generator untouched...this will not scale well if the detector sensitivities change significantly!
"""
        pass
        '''
find maximum distance for detectable things
    -> difficult when we use a non-central chi2 distrib to measure p(det)...
    -> maybe just require cdf(det) to be less than some small fraction --> control the systematic error from this truncation to an acceptable level?

Maybe we just Monte-Carlo at a fixed distance and determine the maximum SNR observed? With that, we can scale the maximum distance based on that fixed distance and the assumption that everything with SNR below some value has negligible probability of being detected
'''

    def update_montecarlo_counters(self, event):
        """update the montecarlo counters for our smart termination condition
        """
        # extract basic params
        pdet = event.pdet

        # compute the effective pdf and jacobian
        # Store PDF value
        pdf = self.pdf(event)

        jac = np.empty_like(self._de1, dtype=float)
        i = 0
        for gen in self._generators:
            jac[i:i+len(gen._params)] = gen.jacobian(*[getattr(event, variate) for variate in gen._variates])
            i += len(gen._params)

        # update the counters
        self._e1 += pdet
        self._e2 += pdet**2
        self._de1 += pdet * jac / pdf
        self._de2 += 2 * pdet**2 * jac / pdf

    def _converged(self, error=np.infty):
        """the smart termination condition. If this is true, then we should stop generating events
        """
        return np.all(2 * self._e1**2 * np.abs(self._de1) * error >= 3 * np.abs(2 * self._e2 * self._de1 - self._e1 * self._de2))

    def generate_events(self, verbose=False, yield_all=False, min_num_samples=0, error=np.infty):
        """Loop and generate events. with_cond=True imposes the MC termination condition presented in the Farr, 2019 RNAAS.
        """
        # Generate a minimum set of samples to determine condition
        for ans in EventGenerator.generate_events(self, n_itr=np.infty, n_event=min_num_samples, verbose=verbose, yield_all=yield_all):
            if yield_all:
                event = ans[1]
            else:
                event = ans
            yield ans

        if error < np.infty:
            for event in self._events:
                self.update_montecarlo_counters(event)

            for ans in EventGenerator.generate_events(self, n_itr=np.infty, n_event=np.infty, verbose=verbose, yield_all=yield_all):
                if yield_all:
                    event = ans[1]
                else:
                    event = ans
                self.update_montecarlo_counters(event)
                yield ans

                if self._converged(error=error): ### check smart termination condition
                    break

    def integrate(self, attr, events=None, sampling_pdf_attr=None):
        """perform the Monte-Carlo integral of "key" with respect to events
        if events is None: use the events stored within this object
        if sampling_pdf_attr is not None: use this attribute at the sampling pdf within the reweighted integral

        returns an estimate of the monte-carlo integral along with an estimate of the variance and the effective number of samples (sum weights)
        """
        ### figure out which events we want to integrate
        if events is None:
            events = self._events

        ### figure out how we want to weigh the samples
        if sampling_pdf_attr is None:
            weights = np.ones(len(events), dtype=float)
        else:
            weights = np.array([self.pdf(event)/getattr(event, sampling_pdf_attr) for event in events], dtype=float)

        ### set up Monte-Carlo integral
        foo = np.array([getattr(event, attr) for event in events], dtype=float)
        return montecarlo_integrate(foo, weights)

    def write2hdf5(*args, **kwargs):
        meta = kwargs.get('meta', dict())
        meta.update({
            "e1": self._e1,
            "e2": self._e2,
            "de1": self._de1,
            "de2": self._de2,
        })
        kwargs['meta'] = meta
        events2hdf5(self._events, self.attributes, *args, **kwargs)

def montecarlo_integrate(foo, weights=None):
    """returns an estimate of the monte-carlo integral along with an estimate of the variance and the effective number of samples (sum weights)
    """
    if weights is None:
        weights = np.ones(len(foo), dtype=float)

    norm = np.sum(weights)

    m1 = np.sum(weights*foo)/norm    ### first moment
    m2 = np.sum(weights*foo**2)/norm ### second moment

    return m1, (m2-m1**2)**0.5, norm
