import numpy as np

import lal

import lalsimulation

#---------------------------------------------------------------------------------------------------
# Event classes
#---------------------------------------------------------------------------------------------------

class Event(object):
    """an object representing a GW event. Knows how to compute the waveform for the event and retains some basic knowledge of event parameters
    """
    _required4waveform = (
        'mass1',
        'mass2',
        'spin1x',
        'spin1y',
        'spin1z',
        'spin2x',
        'spin2y',
        'spin2z',
        'distance',
        'inclination',
        'coa_phase',
        'eccentricity',
    )
    _required = _required4waveform + ('right_ascension', 'declination', 'polarization', 'time')

    _default = dict(
        mass1=1.4,
        mass2=1.4,
        inclination=0.0,
        distance=1.,
        eccentricity=0.0,
        coa_phase=0.0,
        polarization=0.0,
        spin1x=0.0,
        spin1y=0.0,
        spin1z=0.0,
        spin2x=0.0,
        spin2y=0.0,
        spin2z=0.0,
    )

    _waveattrs = ('flow', 'fhigh', 'deltaf', 'fref', 'approximant')

    def __init__(self, **kwargs):
        self._cached = False

        for k, v in kwargs.items():
            setattr(self, k, v)

    def _check(self):
        """check to make sure we have all the required attributes needed for this event
        """
        for k in self._required:
            assert hasattr(self, k), 'Event does not have attribute "%s".\nEvent only has %s.'%(k, ', '.join(dir(self)))

    def _check4waveform(self, use_defaults=True):
        """check to make sure we have all the required attributes needed for waveform generation defined for this event
        """
        for k in self._required4waveform:
            if not hasattr(self, k):
                if use_defaults:
                    setattr(self, k, self._default[k])
                else:
                    raise AttributeError('Event does not have attribute "%s"'%k)

    def del_waveform(self):
        self._cached = False
        for attr in self._waveattrs+('hx','hp'):
            if hasattr(self, attr):
                delattr(self, attr)

    def waveform(self, flow=10.0, deltaf=0.125, fhigh=2048., fref=10., approximant="IMRPhenomPv2", use_defaults=True):
        """
        Generate, and internally store, the h_+ and h_x polarizations for an event.
        """
        if not self._cached:
            self._check4waveform(use_defaults=use_defaults)
            params = None

            try:
                hp, hx = lalsimulation.SimInspiralFD(
                    self.mass1 * lal.MSUN_SI,
                    self.mass2 * lal.MSUN_SI,
                    self.spin1x,
                    self.spin1y,
                    self.spin1z,
                    self.spin2x,
                    self.spin2y,
                    self.spin2z,
                    self.distance * 1e6 * lal.PC_SI,
                    self.inclination,
                    self.coa_phase,
                    0.0,
                    self.eccentricity,
                    0.0,
                    deltaf,
                    flow,
                    fhigh,
                    fref,
                    params,
                    lalsimulation.SimInspiralGetApproximantFromString(approximant),
                )
                self.freqs = np.arange(0, fhigh+deltaf, deltaf)
            except RuntimeError as e:
                print(e)
                print((
                    self.mass1, self.mass2,
                    self.spin1x, self.spin1y, self.spin1z,
                    self.spin2x, self.spin2y, self.spin2z,
                    self.distance, self.inclination,
                    self.coa_phase, 0.0, self.eccentricity, 0.0,
                    deltaf, flow, fhigh, fref, params, approximant
                ))
                raise e

            setattr(self, 'hp', hp)
            setattr(self, 'hx', hx)
            setattr(self, 'flow', flow)
            setattr(self, 'fhigh', fhigh)
            setattr(self, 'deltaf', deltaf)
            setattr(self, 'fref', fref)
            setattr(self, 'approximant', approximant)

        return self.hp.data.data, self.hx.data.data, self.freqs
