#!/usr/bin/env python
import json
import itertools

import numpy

from scipy.special import erfinv
from scipy.stats import lognorm, poisson, expon
from scipy.integrate import fixed_quad, trapz, cumtrapz

from astropy import units
_invmpc3yr = units.Unit("Mpc^-3 yr^-1")
_invgpc3yr = units.Unit("Gpc^-3 yr^-1")

#
# Utilities for generating rate distributions
#

def solve_lognorm_params(low, high):
    """
    Obtain parameters for the scipy lognorm distribution by fitting mu and sigm to the low and high numbers for a given category of rates. Thanks to Will Farr for some guidance in this matter.
    """

    try:
        mu = 0.5 * (numpy.log(high) + numpy.log(low))
        # Nominally, we used 0.25 (2 sigma) here, but this is a better
        # approximation
        sigma = 0.25 * (numpy.log(high) - numpy.log(low))
    except (units.core.UnitTypeError, TypeError):
        mu = 0.5 * (numpy.log(high.value) + numpy.log(low.value))
        # Nominally, we used 0.25 (2 sigma) here, but this is a better
        # approximation
        sigma = 0.25 * (numpy.log(high.value) - numpy.log(low.value))

    dR = numpy.log(high.value) - numpy.log(low.value)
    derf = erfinv(2 * 0.95 - 1) - erfinv(2 * 0.05 - 1)
    sigma = dR / derf

    dR = numpy.log(high.value) + numpy.log(low.value)
    derf = erfinv(2 * 0.95 - 1) + erfinv(2 * 0.05 - 1)
    mu = dR + derf * sigma

    sigma /= numpy.sqrt(2)
    mu /= 2

    return mu, sigma

def generate_rate_prior(low, high):
    # Deprecated
    mu, sigma = solve_lognorm_params(low, high)
    return lognorm(sigma, scale=numpy.exp(mu))

def n_given_rate(r, n, vt):
    _lambda = r * vt
    return poisson(_lambda).pmf(n)

#
# Distributional objects
#

class CitedObject(object):
    def __init__(self, url=None):
        super(CitedObject, self).__init__()
        self._url = url
        # related or superceding
        self._related = set()

    def __lshift__(self, value):
        if not isinstance(value, CitedObject):
            raise TypeError("Attempt to append an object not of type `CitedObject`")

        value._related.add(self)
        return self

    def __ilshift__(self, value):
        if not isinstance(value, CitedObject):
            raise TypeError("Attempt to append an object not of type `CitedObject`")

        value._related.add(self)
        return value

#
# Rate Distributions
#
class RateDistribution(CitedObject):
    def __init__(self, dtype, low=None, high=None, url=None):
        super(RateDistribution, self).__init__(url)

        self._low, self._high = low, high

        # lognormal or UL (exponential like)
        if dtype == "lognorm":
            mu, sigma = solve_lognorm_params(low, high)
            self.distr = lognorm(sigma, scale=numpy.exp(mu))
            try:
                self._range = numpy.logspace(numpy.log10(low) - 1, \
                                    numpy.log10(high) + 1, 100) * _invgpc3yr
            except (units.core.UnitTypeError, TypeError):
                self._range = numpy.logspace(numpy.log10(low.value) - 1, \
                                numpy.log10(high.value) + 1, 100) * _invgpc3yr

        elif dtype == "expon":
            # FIXME: high is assumed 90% CI
            scale = -high / numpy.log(1 - .9)
            self.distr = expon(scale=scale)
            try:
                self._range = numpy.logspace(numpy.log10(scale) - 2, \
                                            numpy.log10(scale) + 2, 100)
            except (units.core.UnitTypeError, TypeError):
                self._range = numpy.logspace(numpy.log10(scale.value) - 2, \
                                numpy.log10(scale.value) + 2, 100) * _invgpc3yr
        else:
            raise NotImplementedError("Distributional type {0} not yet supported.".format(dtype))

    def __str__(self):
        return "{0}: ({1:.2f}, {2:.2f})".format(self.distr.dist.name, \
                                                self._low or 0., self._high)

    def integrate_rates(self, n, vt):

        # This is our rate distribution
        rate_distr = self.distr

        # We're integrating the rate prior against p(n|R), likely a poissionian
        # distribution with the Poisson mean given by RVT
        def integrand(r, n, vt):
            return rate_distr.pdf(r) * n_given_rate(r, n, vt=vt)

        intg = [integrand(self._range, _n, vt) for _n in n]

        p_n = numpy.array([trapz(intg, self._range)]).flatten()

        return p_n, numpy.asarray(intg)

#
# Rate integration
#

def integrate_rates(r, n, rlow, rhigh, vt):
    # Deprecated

    # This is our rate distribution
    rate_distr = generate_rate_prior(rlow, rhigh)

    # We're integrating the rate prior against p(n|R), likely a poissionian
    # distribution with the Poisson mean given by RVT
    def integrand(r, n, vt):
        return rate_distr.pdf(r) * n_given_rate(r, n, vt=vt)

    intg = [integrand(r, _n, vt) for _n in n]

    p_n = numpy.array([trapz(intg, r)]).flatten()

    return p_n, numpy.asarray(intg)

#
# Event based detection probabilities
#

def _prb_n_detect(prb):
    nprb = 1. - prb
    idx = set(range(len(prb)))
    n_prb = numpy.zeros(len(prb)+1)
    n_prb[0] = numpy.prod(nprb)
    n_prb[-1] = numpy.prod(prb)
    for i in range(1, len(prb)):
        # Detection probs
        for comb in itertools.combinations(idx, i):
            det = numpy.asarray(comb)
            # Complement is misses
            miss = numpy.asarray(list(idx - set(det)))
            n_prb[i] += numpy.prod(prb[det]) * numpy.prod(nprb[miss])

    return n_prb

def prb_n_detect(prb):
    """
    The straightforward way of calculating the P(n_obs|{p}_i) is computation unfeasinble for large i \\in N. This is the "discrete Fourier transform" method, explained in https://en.wikipedia.org/wiki/Poisson_binomial_distribution . It is a refinement of a recursive method which is much faster, but numerically unstable.
    """
    n = numpy.arange(0, len(prb)+1)
    c_n = numpy.exp(2j * numpy.pi / len(n))
    c_l = c_n**(n + 1)
    n_prb = 1 + numpy.outer(prb, c_l - 1)
    c_k = numpy.asarray([c_l**-k for k in n])
    n_prb = c_k * n_prb.prod(axis=0)
    return (1. / len(n)) * numpy.real_if_close(n_prb.sum(axis=1))
