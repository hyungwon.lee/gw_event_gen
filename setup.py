#!/usr/bin/env python
__usage__ = "setup.py command [--options]"
__description__ = "standard install script"
__author__ = "Chris Pankow <chris.pankow@ligo.org>, Eve Chase <eve.chase@ligo.org>, Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

from setuptools import (setup, find_packages)
import glob

setup(
    name = 'gw_event_gen',
    version = '0.1.0',
    url = 'https://git.ligo.org/chris-pankow/gw_event_gen',
    author = __author__,
    author_email = 'chris.pankow@ligo.org',
    description = __description__,
    license = 'MIT',
    scripts = ["bin/generate_events"],
    packages = find_packages(),
    data_files = [],
    requires = [],
)
